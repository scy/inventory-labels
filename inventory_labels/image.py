from dataclasses import dataclass

from PIL import Image, ImageDraw


@dataclass
class TapeSize:
    tape_height_px: int
    margin_px: int

    @property
    def print_height_px(self) -> int:
        return self.tape_height_px - (2 * self.margin_px)


TAPE_SIZES = {
    "12mm": TapeSize(84, 7),
}


class Label:
    def __init__(self, size: str, name: str):
        self._size = TAPE_SIZES[size]
        self._image = img = Image.new(
            mode="1",  # black & white
            size=(100, self._size.print_height_px),
            color=1,  # white background
        )
        draw = ImageDraw.Draw(img)
        draw.text((0,0), name)

    def save(self, filename: str):
        self._image.save(filename)


if __name__ == "__main__":
    Label("12mm", "Beispiel").save("example.png")
